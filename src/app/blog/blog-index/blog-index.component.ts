import { Component } from '@angular/core';
import { ScullyRoutesService, ScullyRoute } from '@scullyio/ng-lib';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const BLOG_ROUTE = '/blog/';

@Component({
  selector: 'app-blog-index',
  templateUrl: './blog-index.component.html',
  styleUrls: ['./blog-index.component.scss']
})
export class BlogIndexComponent {
  /**
   * If we use a standard date format in our filenames, it'll sort the posts by
   * date. Just have to reverse them.
   */
  blogRoutes$ = this.scully.available$.pipe(
    map(availableRoutes =>
      availableRoutes
        .filter(routeObject => routeObject.route.startsWith(BLOG_ROUTE))
        .reverse()
    )
  );

  /**
   * If we're using date to sort, might as well use it for showing the date in
   * the list. The alternatives would be no date or duplicating date info in the
   * front matter.
   *
   * This assumes a YYYY-MM-DD format.
   */
  blogRoutesWithDate$: Observable<ScullyRoute[]> = this.blogRoutes$.pipe(
    map(routes =>
      routes.map(route => ({
        ...route,
        date: route.route.substr(BLOG_ROUTE.length, 10)
      }))
    )
  );

  constructor(private scully: ScullyRoutesService) {}
}
